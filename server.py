from flask import Flask, request
from flask_restplus import Api, Resource, fields
from dynamodb import DynamoDB

flask_app = Flask(__name__)
app = Api(app=flask_app)

name_space = app.namespace('replicators', description='Replicator APIs')


@name_space.route('/<string:uuid>')
class SpecifiedResource(Resource):

    @app.doc(responses={ 200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error' },
             params={'uuid': 'Specify the Id associated with the record'})
    def get(self, uuid):
        try:
            db = DynamoDB()
            return db.read_record(uuid)

        except KeyError as e:
            name_space.abort(500, e.__doc__, status="Could not retrieve information", statusCode="500")
        except Exception as e:
            name_space.abort(400, e.__doc__, status="Could not retrieve information", statusCode="400")

    @app.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'uuid': 'Specify the Id associated with the record'})
    def delete(self, uuid):
        try:
            db = DynamoDB()
            return db.delete_record(uuid)

        except KeyError as e:
            name_space.abort(500, e.__doc__, status="Could not delete record", statusCode="500")
        except Exception as e:
            name_space.abort(400, e.__doc__, status="Could not delete record", statusCode="400")


@name_space.route('/')
class UnspecifiedResource(Resource):

    model = app.model(
        'Replicator Model',
        {
            'src': fields.String(
                required=True,
                description="Source chat id",
                help="Source chat id"
            ),
            'dst': fields.String(
                required=True,
                description="Destination chat id",
                help="Destination chat id"
            ),
            'description': fields.String(
                required=True,
                description="Chat description",
                help="Chat description"
            )
        }
    )

    @app.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'})
    @app.expect(model)
    def post(self):
        try:
            db = DynamoDB()
            src = request.json['src']
            dst = request.json['dst']
            description = request.json['description']
            uuid = db.create_record(src, dst, description)
            return {
                "status": "New record added",
                "name": uuid
            }

        except KeyError as e:
            name_space.abort(500, e.__doc__, status="Could not save information", statusCode="500")
        except Exception as e:
            name_space.abort(400, e.__doc__, status="Could not save information", statusCode="400")

    @app.doc(responses={ 200: 'OK', 400: 'Table does not exist', 500: 'Retrieve information error'})
    def get(self):
        try:
            db = DynamoDB()
            return db.read_records()

        except db.client.exceptions.ResourceNotFoundException as e:
            name_space.abort(400, e.__doc__, status="Table does not exist", statusCode="400")
        except Exception as e:
            name_space.abort(500, e.__doc__, status="Could not retrieve information", statusCode="500")


@name_space.route('/table')
class Table(Resource):

    @app.doc(responses={ 200: 'OK', 400: 'Table does not exist', 500: 'Table check error'})
    def get(self):
        try:
            db = DynamoDB()
            return db.describe_table()
        except db.client.exceptions.ResourceNotFoundException as e:
            name_space.abort(400, e.__doc__, status="Table does not exist", statusCode="400")
        except Exception as e:
            name_space.abort(500, e.__doc__, status="Table check error", statusCode="500")

    @app.doc(responses={ 200: 'OK', 400: 'Table delete error'})
    def delete(self):
        try:
            db = DynamoDB()
            return db.delete_table()

        except Exception as e:
            name_space.abort(400, e.__doc__, status="Table delete error", statusCode="400")

    @app.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'})
    def post(self):
        try:
            db = DynamoDB()
            response = db.create_table_if_not_exists()
            return response

        except KeyError as e:
            name_space.abort(500, e.__doc__, status="Could not create table", statusCode="500")
        except Exception as e:
            name_space.abort(400, e.__doc__, status="Could not create table", statusCode="400")

    # @app.doc(responses={ 200: 'OK', 500: 'Retrieve information error'})
    # def get(self):
    #     try:
    #         db = DynamoDB()
    #         return db.read_records()
    #
    #     except Exception as e:
    #         name_space.abort(500, e.__doc__, status="Could not retrieve information", statusCode="500")
