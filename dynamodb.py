from __future__ import print_function # Python 2/3 compatibility
import boto3
import decimal
import datetime
import uuid as uuid_gen
from tools import jsonify, jsonify2
import json


class DynamoDB:

    def __init__(self):
        self.table_name = 'Replicators'
        self.db = boto3.resource('dynamodb',
                                 aws_access_key_id="fakeMyKeyId",
                                 aws_secret_access_key="fakeSecretAccessKey",
                                 region_name='eu-central-1',
                                 endpoint_url="http://localhost:8000")
        self.table = self.db.Table(self.table_name)
        self.client = boto3.client('dynamodb',
                                   aws_access_key_id="fakeMyKeyId",
                                   aws_secret_access_key="fakeSecretAccessKey",
                                   region_name='eu-central-1',
                                   endpoint_url="http://localhost:8000")

    def describe_table(self):
        response = self.client.describe_table(TableName=self.table_name)
        return str(response)

    def delete_table(self):
        response = self.client.delete_table(TableName=self.table_name)
        return str(response)

    def create_table_if_not_exists(self):
        try:
            self.describe_table()
            return {
                "table_status": "Table already exists"
            }
        except self.client.exceptions.ResourceNotFoundException:
            # Table does not exist
            table = self.db.create_table(
                TableName=self.table_name,
                KeySchema=[
                    {
                        'AttributeName': 'uuid',
                        'KeyType': 'HASH'  # Partition key
                    }
                ],
                AttributeDefinitions=[
                    {
                        'AttributeName': 'uuid',
                        'AttributeType': 'S'
                    }
                ],
                ProvisionedThroughput={
                    'ReadCapacityUnits': 1,
                    'WriteCapacityUnits': 1
                }
            )
            return {
                "table_status:": table.table_status
            }

    def create_record(self, src, dst, description):
        uuid = str(uuid_gen.uuid1())
        created = datetime.datetime.now().isoformat()
        self.table.put_item(
            Item={
                'uuid': uuid,
                'src': src,
                'dst': dst,
                'description': description,
                'created': created
            }
        )
        return uuid

    def create_record_from_file(self):
        with open("data.json") as json_file:
            record = json.load(json_file, parse_float=decimal.Decimal)

            uuid = str(uuid_gen.uuid1())
            src = int(record['src'])
            dst = record['dst']
            description = record['description']
            created = datetime.datetime.now().isoformat()

            self.table.put_item(
                Item={
                    'uuid': uuid,
                    'src': src,
                    'dst': dst,
                    'description': description,
                    'created': created
                }
            )

    def read_record(self, uuid):
        response = self.table.get_item(
            Key={
                'uuid': uuid
            }
        )
        return jsonify(response['Item'])

    def read_records(self):
        return jsonify(self.table.scan()['Items'])

    def delete_record(self, uuid):
        response = self.table.delete_item(
            Key={
                'uuid': uuid
            }
        )
        return jsonify(response)
