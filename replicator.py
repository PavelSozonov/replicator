from telethon import TelegramClient, events, sync
import configparser
import argparse
import socks
import sys
import traceback
import logging

class Replicator():

    def __init__(self, api_id, api_hash, sender_id, receiver_id, use_proxy):
        self.sender_id = sender_id
        self.receiver_id = receiver_id
        app_name = __file__.split('/')[-1].split('.')[0]
        if use_proxy:
            self.client = TelegramClient(app_name, api_id, api_hash, proxy=(socks.SOCKS5, '127.0.0.1', 4444))
        else:
            self.client = TelegramClient(app_name, api_id, api_hash)

    def run_client(self):
        
        self.client.start()
        
        @self.client.on(events.NewMessage())
        async def handler(event):
            if str(self.sender_id) in str(event.to_id):
                print(event.date)
                try:
                    await event.forward_to(int(self.receiver_id))
                except ValueError:
                    print('Forward error, try to process it')
                    traceback.print_exc(file=sys.stdout)
                    await self.client.get_dialogs()
                    print('Send again')
                    await event.forward_to(int(self.receiver_id))
                    print('Sent')

        self.client.run_until_disconnected()

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--profile", "-p", action="store", dest="profile", help="profile name (default: prod)", default="prod")
    parser.add_argument("--proxy", "-x", action="store_true", dest="use_proxy", help="use socks proxy (via ssh tunnel 127.0.0.1:4444)")
    parser.add_argument("--debug", "-d", action="store_true", dest="debug", help="use debug logging")
    args = parser.parse_args()
    profile = args.profile
    use_proxy = args.use_proxy

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    configParser = configparser.RawConfigParser()   
    configFilePath = r'application.properties'
    configParser.read(configFilePath)

    # api_id and api_hash from https://my.telegram.org, under API Development.
    api_id = configParser.get('default', 'api_id')
    api_hash = configParser.get('default', 'api_hash')

    sender_id = configParser.get(profile, 'sender')
    receiver_id = configParser.get(profile, 'receiver')

    Replicator(api_id, api_hash, sender_id, receiver_id, use_proxy).run_client()
