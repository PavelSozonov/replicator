import simplejson as json


def jsonify(obj):
    return json.loads(json.dumps(obj))


def jsonify2(obj):
    return json.dumps(obj)
